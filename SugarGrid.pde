import java.lang.Math;
import java.util.HashSet;
import java.util.Collections;
import java.util.Random;

class SugarGrid {

  private Square[][] grid;
  private int howWide;
  private int howHigh;
  private int squareSideLength;
  private GrowthRule growthRule;
  private ReplacementRule replaceRule;
  private FertilityRule   fertilityRule;
  private Random rand;

  /* Initializes a new SugarGrid object with a w*h grid of Squares, 
   *  a sideLength for the squares (used for drawing purposes only) 
   *  of the specified value, and 
   *  a sugar growback rule g. 
   *  Initialize the Squares in the grid to have 0 initial and 0 maximum sugar.
   *
   */
  public SugarGrid(int w, int h, int sideLength, GrowthRule g,ReplacementRule rr,FertilityRule fr) {
    this.howWide = w;
    this.howHigh = h;
    this.squareSideLength = sideLength;
    growthRule = g;
    this.replaceRule = rr;
    this.fertilityRule = fr;
    rand = new Random();

    // make the grid, initially with 0-max-sugar Squares
    grid = new Square[howWide][howHigh];
    for (int i = 0; i < howWide; i++) {
      for (int j = 0; j < howHigh; j++) {
        grid[i][j] = new Square(0, 0, i, j);
      }
    }
  }

  /* Accessor methods for the named variables.
   *
   */

  /* Accessors
   */
  public int getWidth() {
    return howWide;
  }

  public int getHeight() {
    return howHigh;
  }

  public int getSquareSize() {
    return squareSideLength;
  }

  /* returns respectively the initial or maximum sugar at the Square 
   *  in row i, column j of the grid.
   *
   */
  public int getSugarAt(int i, int j) {
    assert(i >= 0 && j >= 0 && i < howWide && j < howHigh);
    return grid[i][j].getSugar();
  }

  public int getMaxSugarAt(int i, int j) {
    assert(i >= 0 && j >= 0 && i < howWide && j < howHigh);
    return grid[i][j].getMaxSugar();
  }

  /* returns the Agent occupying the square at position (i,j) in the grid, 
   *  or null if no agent is present there.
   *
   */
  public Agent getAgentAt(int i, int j) {
    assert(i >= 0 && j >= 0 && i < howWide && j < howHigh);
    return grid[i][j].getAgent();
  }

  /* places Agent a at Square(i,j), provided that the square is empty. 
   *  If the square is not empty (and doesn't contain a), the program should crash with an assertion failure.
   *
   */
  public void placeAgent(Agent a, int i, int j) {
    assert(i >= 0 && j >= 0 && i < howWide && j < howHigh);
    Square s = grid[i][j];
    if (s.getAgent() == null) {
      s.setAgent(a);
      a.setSquare(s);
    }
    assert(s.getAgent().equals(a));
  }

  /* A method that computes the Euclidian distance between two squares on the grid 
   *  at (x1,y1) and (x2,y2). 
   *  Points are indexed from (0,0) up to (width-1, height-1) for the grid. 
   *  The formula for Euclidean distance is normally sqrt( (x2-x1)2 + (y2-y1)2 ) However...
   *  As in the book, the grid is a torus. 
   *  This means that an Agent that moves off the top of the grid ends up at the bottom 
   *  (and vice versa), and 
   *  an Agent that moves off the left hand side of the grid ends up on the right hand 
   *  side (and vice versa). 
   */
  public double euclideanDistance(Square s1, Square s2) {
    int xDiff = Math.abs(s1.getX() - s2.getX());
    int yDiff = Math.abs(s1.getY() - s2.getY());
    xDiff = Math.min(xDiff, howWide - xDiff);
    yDiff = Math.min(yDiff, howHigh - yDiff);
    return Math.sqrt(Math.pow(xDiff, 2) + Math.pow(yDiff, 2));
  }

  /* Creates a circular blob of sugar on the grid. 
   *  The center of the blob is at position (x,y), and 
   *  that Square is updated to store a maximum of max sugar or 
   *  its current maximum value, whichever is greater. 
   *
   *  Then, every square within euclidian distance of radius is updated 
   *  to store a maximum of (max-1) sugar, or its current maximum value, 
   *  whichever is greater. 
   *
   *  Then, every square within euclidian distance of 2*radius is updated 
   *  to store a maximum of (max-2) sugar, or its current maximum value, 
   *  whichever is greater. 
   *
   *  This process continues until every square has been updated. 
   *  Any Square that has a new maximum value 
   *  should also have its Sugar level set to this maximum.
   *
   */
  public void addSugarBlob(int x, int y, int radius, int max) {
    Square xy = new Square(0, 0, x, y);
    for (int i = 0; i < howWide; i++) {
      for (int j = 0; j < howHigh; j++) {
        Square s = grid[i][j];
        int radii = (int) Math.ceil(euclideanDistance(s, xy)/radius);
        s.setSugar(s.getSugar() + Math.max(0, max - radii), true);
      }
    }
  }

  /* Returns a linked list containing radius squares in each cardinal direction, 
   *  centered on (x,y). 
   *
   *  For example, generateVision(5,5,2) should return the squares 
   *   (5,5), (4,5), (3,5), (6,5), (7,5), (5,4), (5,3), (5,6), and (5,7). 
   *
   *  returns all of these points that are on the grid; if radius < 0 returns an empty list  
   *
   *  When radius is 0, returns a list containing only (x,y). 
   *
   */
  public LinkedList<Square> generateVision(int x, int y, int radius) {
    LinkedList<Square> retval = new LinkedList<Square>();
    if (radius < 0) {
      return retval;
    }
    for (int i = -radius; i <= radius; i++) {
      if (y+i >= 0 && y+i < howHigh && x >= 0 && x < howWide) {
        retval.add(grid[x][y+i]);
      }
      if (x+i >= 0 && x+i < howWide && i != 0 && y >= 0 && y < howHigh) {
        retval.add(grid[x+i][y]);
      }
    }
    return retval;
  }

  /* Adds agent at a Square
   */
  public void addAgentAt(Agent ag, int i, int j) {
    grid[i][j].setAgent(ag);
  }

  /* Updates the grid by one step. Each square on the grid is processed in turn, according the following steps:
   * 1. The GrowbackRule of this grid is applied to the Square, possibly increasing its sugar level.
   * 2. If the square is not occupied, or is newly occupied by an agent that moved to this square during 
   *    this call to update(), then we're done and can go to the next square.
   * 3. If the square has an agent in it, then:
   *   a. The agent burns its stored sugar based on its metabolic rate.
   *   b. If the agent is now dead, mark its current square as unoccupied.
   *   c. If the agent is still alive, generate vision for the agent (based on the agent's vision radius)
   *   d. Apply the agent's movement rule to determine where the agent wants to move.
   *   e. Move the agent to its preferred square, provided the target square is not occupied.
   *   f. Make the agent eat all the sugar on the current square.
   *
   * New for A6: uses a HashSet instead of an grid of booleans to track newly occupied Squares 
   *             (so as not to let an agent move twice in one udpate)
   */
  public void update() {
    HashSet<Square> seenSquares = new HashSet<Square>();
    for (int i = 0; i < howWide; i++) {
      for (int j = 0; j < howHigh; j++) {
        Square s = grid[i][j];
        growthRule.growBack(s);
        if (seenSquares.contains(s)) {
          continue;
        }
        Agent a = s.getAgent();
        if (a == null) {
          continue;
        }
        a.step(); 
        if(replaceRule.replaceThisOne(a) || !a.isAlive()) {
         // a = replaceRule.replace(a,null);
          s.setAgent(null);
          continue;
         // s.setAgent(a);
        }
        LinkedList<Square> vision = generateVision(i, j, a.getVision());
        Square dest = a.getMovementRule().move(vision, this, s);

        if (dest.getAgent() == null) {
          a.move(s, dest);
          seenSquares.add(dest);
        }
        a.eat();
        if(fertilityRule.isFertile(a)){
          vision = generateVision(i, j, 1);
          for(Square sq: vision){
            Agent b = sq.getAgent();
            if (b != null && fertilityRule.canBreed(a, sq.getAgent(),vision))
               fertilityRule.breed(a,sq.getAgent(),vision,generateVision(sq.getX(),sq.getY(),1));     
          }  
        }
      }
    }
  }
  /* Display each square
   */
  public void display() {
    for (int i = 0; i < howWide; i++) {
      for (int j = 0; j < howHigh; j++) {
        grid[i][j].display(squareSideLength);
      }
    }
  }

  /* inserts agent a at a randomly selected position on the grid. 
   * Puts the agent at the first unoccupied "random" position. Following these instructions: 
   * The SugarGrid stores a randomly shuffled list of all square positions and cycles through the list
   *
   * Does nothing if an unoccupied Square can't be found
   */
  public void addAgentAtRandom(Agent a) {
    Square s = getRandomUnoccupiedSquare();
    s.setAgent(a);
  }

  /* Gets a random unoccupied square 
   * Does it by picking a random i, j; if there's an agent at that square, it tries again.
   * Returns null after nlogn tries, n = #squares
   */
  private Square getRandomUnoccupiedSquare() {
    int limit = (int) (howWide*howHigh*Math.log(howWide*howHigh));
    for (int n = 0; n < limit; n++) {
      int i = rand.nextInt(howWide);
      int j = rand.nextInt(howHigh);
      Square s = grid[i][j];
      if (s.getAgent() == null) {
        return s;
      }
    }
    return null;
  }
  
  /* returns a list of all agents on the SugarGrid at present.
  *
  */
  public ArrayList<Agent> getAgents() {
    ArrayList<Agent> retval = new ArrayList<Agent>();
    for (int i = 0; i < howWide; i++) {
      for (int j = 0; j < howHigh; j++) {
        Agent a = grid[i][j].getAgent();
        if (a != null) {
          retval.add(a);
        }
      }
    }
    return retval;
  }
  
public void killAgent(Agent a){
   /*
    sets the sugarLevel of an Agent to 0, 
    and passes the dead Agent to the FertilityRule and ReplacementRule that are stored in the SugarGrid.
  */
  
  a.addSugar(-(a.getSugarLevel()));  // add negative current level to take to 0
  fertilityRule.isFertile(a);
  replaceRule.replaceThisOne(a); 
 }  
  
}


class SugarGridTester {
  void test() {
  int numAgents = 20;
  int minMetabolism = 3;
  int maxMetabolism = 6;
  int minVision = 3;
  int maxVision = 6;
  int minInitialSugar = 5;
  int maxInitialSugar = 10;
  MovementRule mr = new PollutionMovementRule();
  AgentFactory af = new AgentFactory(minMetabolism, maxMetabolism, minVision, maxVision, 
    minInitialSugar, maxInitialSugar, mr);    
    GrowbackRule gr = null;
    int w = 20;
    int h = 20;
    int sideLength = 15;
  int minAge = 40;
  int maxAge = 75;
  ReplacementRule replaceRule = new ReplacementRule(minAge, maxAge, af);
  Map<Character, Integer[]> childbearingOnset = new HashMap<Character, Integer[]>();
  Map<Character, Integer[]> climactericOnset = new HashMap<Character, Integer[]>();
  Integer[] maleBearingOnset = {new Integer(14),new Integer(18)};
  Integer[] femaleBearingOnset = {new Integer(12),new Integer(17)};
  childbearingOnset.put('X',maleBearingOnset);
  childbearingOnset.put('Y',femaleBearingOnset);
  Integer[] maleClimactericOnset = {new Integer(14),new Integer(18)};
  Integer[] femaleClimactericOnset = {new Integer(12),new Integer(17)};
  climactericOnset.put('X',maleClimactericOnset);
  climactericOnset.put('Y',femaleClimactericOnset);
  FertilityRule fr = new FertilityRule(childbearingOnset,  climactericOnset);
    // constructor, accessors  
  SugarGrid sg = new SugarGrid(20, 20, 15, gr,replaceRule,fr);



    assert(sg.getWidth() == 20);
    assert(sg.getHeight() == 20);
    assert(sg.getSquareSize() == 15);

    assert(sg.getSugarAt(0, 1) == 0);
    assert(sg.getMaxSugarAt(0, 1) == 0);
    assert(sg.getAgentAt(0, 1) == null);

    // add sugar blob
    int x = 0;
    int y = 1;
    int radius = 1;
    int max = 2;
    sg.addSugarBlob(x, y, radius, max);
    assert(sg.getSugarAt(0, 1) == 2);
    assert(sg.getSugarAt(0, 0) == 1);
    assert(sg.getSugarAt(1, 0) == 0);

    // distance
    Square s1 = new Square(5, 9, 10, 10);
    Square s2 = new Square(5, 9, 13, 14);
    assert(sg.euclideanDistance(s1, s2) == 5.0d);

    // vision
    LinkedList<Square> ll = sg.generateVision(1, 3, 4);
    assert(ll.size() == 13);

    // place agents
    int metabolism = 3;
    int vision = 2;
    int initialSugar = 4;

    /* display
     Agent a01 = new Agent(metabolism, vision, initialSugar, new PollutionMovementRule());
     Agent a10 = new Agent(metabolism, vision, initialSugar, new PollutionMovementRule());
     sg.placeAgent(a01, 0, 1);
     sg.placeAgent(a10, 1, 0);
     sg.display();
     */

    // add agents at random
    Agent a1 = new Agent(metabolism, vision, initialSugar, new PollutionMovementRule());
    Agent a2 = new Agent(metabolism, vision, initialSugar, new PollutionMovementRule());
    sg.addAgentAtRandom(a1);
    sg.addAgentAtRandom(a2);
    assert(!a1.getSquare().equals(a2.getSquare()));
    assert(a1.getSquare().getAgent() == a1);
    assert(a2.getSquare().getAgent() == a2);
  }
}
