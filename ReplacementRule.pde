import java.util.Collections;
import java.util.Random;
import java.util.List;
import java.util.HashMap;

class ReplacementRule {
  private int minAge;
  private int maxAge;
  public HashMap<Agent, Integer> agentsLifeSpans;
  private Random r = new Random();
  AgentFactory fac;
  
  /* initializes a replacement rule with the specified values for minimum and maximum lifespans. 
  * fac will be used to generate any new random agents.
  */
  public ReplacementRule(int minAge, int maxAge, AgentFactory fac) {
    this.minAge = minAge;
    this.maxAge = maxAge;
    this.fac = fac;
    this.agentsLifeSpans = new HashMap<Agent, Integer>();
  }
  
  /* This method accepts an Agent a, and determines whether the agent should be replaced yet. 
  * a should be replaced if it is no longer alive. 
  *
  * Further, if this is the first time ReplacementRule has been asked about Agent a, 
  * then generate a random integer between minAge and maxAge to represent the lifespan of a.
  *
  */
  public boolean replaceThisOne(Agent a) {
    /*
    if (a.getLifespan() == Agent.NOLIFESPAN) {
      Random r = new Random();
      a.setLifespan(minAge + r.nextInt(maxAge+1 - minAge));
    }
    else {
      if (a.getAge() > a.getLifespan()) {
        a.setAge(maxAge+1);
        return true;
      }
    }
    */
    if (a == null){
      return false;
    }  
    Integer lifeSpan;
    if (agentsLifeSpans.containsKey(a)) {
       lifeSpan = agentsLifeSpans.get(a); 
       if (a.getAge() > lifeSpan) {
         a.setAge(maxAge+1);
         return true;
       }  
    }else{
      // minimum + rn.nextInt(maxValue - minvalue + 1)
      lifeSpan = new Integer(minAge + r.nextInt(maxAge - minAge + 1));      
      agentsLifeSpans.put(a,lifeSpan);
    }

    return false;
  }
  
  /* Returns a new Agent that is a replacement for Agent a. 
  */
  public Agent replace(Agent a, List<Agent> others) {
    return fac.makeAgent();
  }
}

class ReplacementRuleTester {
  public void test() {
    int minMetabolism = 3;
    int maxMetabolism = 6;
    int minVision = 2;
    int maxVision = 4;
    int minInitialSugar = 5;
    int maxInitialSugar = 10;
    MovementRule mr = new PollutionMovementRule();
    
    AgentFactory af = new AgentFactory(minMetabolism, maxMetabolism, minVision, maxVision, 
                                       minInitialSugar, maxInitialSugar, mr);
                                      
    int minAge = 40;
    int maxAge = 80;
    ReplacementRule rr = new ReplacementRule(minAge, maxAge, af);
                                       
    Agent a = af.makeAgent();
    
    assert(rr.replaceThisOne(a) == false);
//    int ls = a.getLifespan();
//    assert(ls <= maxAge && ls >= minAge);
    a.step();
//    a.setLifespan(a.getAge() - 1);
    rr.agentsLifeSpans.put(a,a.getAge() - 1);
    assert(rr.replaceThisOne(a) == true);
    List<Agent> others = null;
    a = rr.replace(a, others);
    assert(rr.replaceThisOne(a) == false);    
  }
}
