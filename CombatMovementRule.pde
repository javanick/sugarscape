import java.util.LinkedList;
import java.util.Iterator;

public class CombatMovementRule extends SugarSeekingMovementRule{
  
  private int alpha;
   
  //  Initializes a new CombatMovementRule with the specified value of alpha.
  public CombatMovementRule(int alpha){
    super();
    this.alpha = alpha;
  }  
  
  // Move the agent 
  public Square move(LinkedList<Square> neighbourhood, SugarGrid g, Square middle){
   
    Agent otherAgent;
    Square otherSquare;
    neighbourhood.remove(middle);  // get rid of middle from neighbourhood
    // need to use a copy to iterate through since we will be removing elements
    LinkedList<Square> workingCopy = (LinkedList<Square>) neighbourhood.clone();
    
    for (Iterator<Square> i = workingCopy.iterator();i.hasNext();){
      otherSquare = i.next();
        if(otherSquare.getAgent() != null){ // check if empty
          // Remove from neighbourhood any Square containing an Agent of the same tribe as the Agent on the middle Square.
          if (otherSquare.getAgent().getTribe()==middle.getAgent().getTribe())
            neighbourhood.remove(otherSquare);
          // Remove from neighbourhood any Square containing an Agent that has 
          //  at least as much sugar a the agent on the middle square.  
          else if (otherSquare.getAgent().getSugarLevel() >= middle.getAgent().getSugarLevel())
            neighbourhood.remove(otherSquare);
        }       
    } 
    removeStrongerEnemies(neighbourhood,g,middle);
    increaseSugarInNeighbourhood(neighbourhood);
    /*
    Call the superclass movement method on what's left of neighbourhood, 
    and, if necessary, determine which original square corresponded to a replacement square (if that's what was returned). 
    The original and replacement squares will have the same x and y coordinates. 
    Store the result (which must be one of the original squares in neighbourhood) in target.
    */
    Square replacement = super.move(neighbourhood,g,middle);
    Square target = findSquareWithSameXandY(neighbourhood,replacement.getX(),replacement.getY());
    if (replacement.getAgent() == null)
      return target;
    else{
      /*
      -- Otherwise, store the occupying agent as casualty.
      -- Remove casualty from its Square.
      -- Increase the wealth of this agent by the minimum of casualty's sugarlevel  and alpha
      -- Call the SugarGrid's killAgent() method on casualty.
      -- Return target.
      */
      Agent casualty = replacement.getAgent();
      replacement.setAgent(null);;
      middle.getAgent().addSugar(min(alpha,casualty.getSugarLevel()));
      g.killAgent(casualty);
      return target;
    }  
  }
  
  
  /*
      For each remaining Square in neighbourhood that contains an Agent,
        get the vision that the Agent on middle would have if it moved to that Square. 
        If the vision contains any Agent with more sugar than the Agent on middle, and is of the opposite tribe, 
        then remove the Square in question from neighbourhood.
  */
  
  private void removeStrongerEnemies(LinkedList<Square> neighbourhood,SugarGrid g, Square middle){
 
    LinkedList<Square> workingCopy = (LinkedList<Square>) neighbourhood.clone();
    LinkedList<Square> potentialVisibleSquares = new LinkedList<Square>();
    int radius = middle.getAgent().getVision();
    int agentOnMiddleSugarLevel = middle.getAgent().getSugarLevel();
    boolean agentOnMiddleTribe = middle.getAgent().getTribe();
    for (Iterator<Square> i = workingCopy.iterator();i.hasNext();){
        Square nextNeighbour = i.next();
        if(nextNeighbour.getAgent() != null){
          potentialVisibleSquares = (g.generateVision(nextNeighbour.getX(), nextNeighbour.getY(),radius));
          if (potentialVisibleSquares != null){
            potentialVisibleSquares.remove(middle);
            for(Iterator<Square> n = potentialVisibleSquares.iterator();n.hasNext();){
              Square nextCandidate = n.next();
              if((nextNeighbour.getAgent() != null) 
                 && agentOnMiddleSugarLevel < nextCandidate.getAgent().getSugarLevel() 
                 && agentOnMiddleTribe != nextCandidate.getAgent().getTribe() ){
                   neighbourhood.remove(nextNeighbour);
                   break;
             }  
           }     
          }
        }
    } 
    
  }
  
  
  /*
  Replace each Square in neighbourhood that still has an Agent with a new Square that has the same x and y coordinates, 
  but a Sugar and MaximumSugar level that are increased by the minimum of alpha and the sugar level of the occupying agent.
  */
  private void increaseSugarInNeighbourhood(LinkedList<Square> neighbourhood){
    LinkedList<Square> workingCopy = (LinkedList<Square>) neighbourhood.clone();

    for (Iterator<Square> n = workingCopy.iterator();n.hasNext();){
     Square oldSquare = n.next();
     if(oldSquare.getAgent() != null){
       int sugarLevel = oldSquare.getSugar() + min(oldSquare.getAgent().getSugarLevel(),alpha);
       int maxSugar =+ oldSquare.getMaxSugar() +  min(oldSquare.getAgent().getSugarLevel(),alpha);
       Square sq = new Square(sugarLevel,maxSugar, oldSquare.getX(), oldSquare.getY()); 
       neighbourhood.remove(oldSquare);
       neighbourhood.add(sq);
     }
    }     
  }  
  
 private Square findSquareWithSameXandY(LinkedList<Square> neighbourhood,int x, int y){
    for(Square s : neighbourhood){
      if(s.getX()==x && s.getY()==y)
        return s;
    }
    return null;
 }   
  
}  
