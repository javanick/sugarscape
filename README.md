Sugarscape is a multiagent system that models social interactions based
on a set of rules. This is an object oriented model based on the book
"Growing Artificial Societies", by Epstein and Axtell.

To run the program, you will need to download [Processing](https://processing.org/),
an open source sketchbook based in Java.

You can read more about Sugarscape on [Wikipedia](https://en.wikipedia.org/wiki/Sugarscape):