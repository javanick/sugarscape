import java.util.Collections;
import java.util.Random;
import java.util.HashMap;
import java.util.Map;
import java.util.ListIterator;
import java.util.ArrayList;
class FertilityRule {

  private Map<Character, Integer[]> childbearingOnset;
  private Map<Character, Integer[]> climactericOnset;
  private HashMap<Agent,FertilityNode> childBearingAges;
  private Random r = new Random();

  
  /*  Initializes a new FertilityRule with the specified ages for the start of the fertile and infertile periods for agents of each sex. 
      For example, the map {'X' -> [12,15], 'Y' ->[12,15]} might be used to indicate that the start of the 
      childbearing period is between 12 and 15 years (inclusive) for both sexes.
  */
  public FertilityRule(Map<Character, Integer[]> childbearingOnset, Map<Character,Integer[]> climactericOnset){
    this.climactericOnset = climactericOnset;
    this.childbearingOnset = childbearingOnset;
    this.childBearingAges = new HashMap<Agent,FertilityNode>();
  }  
  /*  Determines whether Agent a is fertile */
  public boolean isFertile(Agent a){
    /* If a is null or dead, then remove all records of it from any storage it may be present in, and return false. */
    if (a == null) return false;
    if (!a.isAlive()){
      // remove all records of agent 
      return false;
    } 
   //  if this is the first time a was passed to this function:
    FertilityNode fn = new FertilityNode();
    if (!childBearingAges.containsKey(a)){
       /* Generate a random number for the onset of childbearing age (c) 
          and the age of the start of a's climacteric (o), 
       */
       int minAge = 0;
       int maxAge = 0;

       Character gender = a.getSex();
       Integer[] bearingOnsetRange = childbearingOnset.get(gender); 
       Integer[] climactericOnsetRange = climactericOnset.get(gender); 
       minAge = bearingOnsetRange[0];
       maxAge = bearingOnsetRange[1];
       // minimum + rn.nextInt(maxValue - minvalue + 1)
       fn.setC(minAge + r.nextInt(maxAge - minAge+1));

       minAge = climactericOnsetRange[0];
       maxAge = climactericOnsetRange[1];
       // minimum + rn.nextInt(maxValue - minvalue + 1)
       fn.setO(minAge + r.nextInt(maxAge - minAge+1));
       fn.setSugarLevel(a.getSugarLevel());              
       childBearingAges.put(a,fn);
    }       
/*
     return true only if:
    c <= a.getAge() < o, using the values of c and o that were stored for this agent earlier.
    a currently has at least as much sugar as it did the first time we passed it to this function.
*/
    fn = childBearingAges.get(a);
    if ((fn.getC() <= a.getAge()) && (a.getAge() < fn.getO())
      && (a.getSugarLevel()>=fn.getSugarLevel())){
        return true;
    }
    return false;
  }
  
/*
    Determines whether the two passed agents can form a breeding pair or not.
    local is the radius 1 vision around agent a.
*/
  public boolean canBreed(Agent a, Agent b, LinkedList<Square> local){
   /*
    a is fertile.
    b is fertile.
    a and b are of different sexes.
    b is on one of the Squares in local.
    At least one of the Squares in local is empty. 
  */
   if ((isFertile(a) && isFertile(b))  && (a.getSex() != b.getSex())){
     ListIterator<Square> i = local.listIterator();
     boolean emptyAvailable = false;
     boolean mateOnLocalSquare = true;
     while(i.hasNext() && !emptyAvailable ){
      Square s = i.next();
      Agent squareAgent = s.getAgent();
      if (squareAgent == null)
        emptyAvailable = true;
  //    else if (squareAgent == b)
  //      mateOnLocalSquare = true;
     }

    return (emptyAvailable && mateOnLocalSquare);       
   }
   return false;
  }
/*
  Creates and places a new Agent that is the offspring of a and b, 
  according to the process below. 
  The local visions of each agent are provided.
*/

public Agent breed(Agent a, Agent b, LinkedList<Square> aLocal, LinkedList<Square> bLocal){
  if(canBreed(a,b,aLocal) && canBreed(b,a,bLocal)){
/*
    Pick one of the parents' metabolisms, uniformly at random.
    Pick one of the parents' visions, uniformly at random.
    Take the MovementRule from Agent a.
    Pick a sex uniformly at random.
*/

  int metabolism = (get0or1()==0) ? a.getMetabolism():b.getMetabolism();
  int vision =  (get0or1()==0) ? a.getVision():b.getVision();
  MovementRule mr = a.getMovementRule();
  char gender = (get0or1()==0) ? 'X' : 'Y';
  Agent child = new Agent(metabolism, vision, 0, mr, gender);
  a.gift(child,a.getSugarLevel()/2);
  b.gift(child,b.getSugarLevel()/2);    
  // a newly created Agent is nurtured by its two parents before it starts moving around the world
  child.nurture(a,b);

//  Pick a random square from aLocal or bLocal that does not have an Agent on it, and place the child on that square.
  //find allopen squares for both parents
   
  ArrayList<Square> l = new ArrayList();
  ListIterator<Square> i = aLocal.listIterator();
  Square s;
  while (i.hasNext()){
    s = i.next();
    if (s.getAgent() == null)
      l.add(s);
  }
  i = bLocal.listIterator();
  while (i.hasNext()){
    s = i.next();
    if (s.getAgent() == null)
      l.add(s);
  } 
  int maxEmptySquares = l.size();
  int k = r.nextInt(maxEmptySquares);
  s = l.get(k);
  s.setAgent(child);
  
  return child;
  } 
return null;
}
 
private int get0or1(){
  return r.nextInt(2);
} 
  
}
