import java.lang.Math;
import java.util.Random;


class Agent implements Comparable<Agent> {
  public static final int NOLIFESPAN = -999;
  public static final int MAXWIDTH = 1000; // for use in compareTo()
  private int metabolism;
  private int vision;
  private int sugarLevel;
  private MovementRule movementRule;
  private int age;
//  private int lifespan;
  private Square square;
  private int[] fillColor;
  private SocialNetworkNode snNode;
  private char sex;
  private boolean[] culture ;
  private Random rand = new Random();
  private  int[] blue = {0, 0, 255};
  private int[] red = {255, 0, 0}; // in case of collision
  
  /* initializes a new Agent with the specified values for its 
  *  metabolism, vision, stored sugar, and movement rule.
  *
  */
  public Agent(int metabolism, int vision, int initialSugar, MovementRule m) {
    this.metabolism = metabolism;
    this.vision = vision;
    this.sugarLevel = initialSugar;
    this.movementRule = m;
    this.sex = rand.nextBoolean() ? 'X' : 'Y'; 
    age = 0;
 //   lifespan = NOLIFESPAN;
    square = null;
    int[] tmp = {0, 0, 0};
    fillColor = tmp;
    snNode = null;
    initCulture();
  }
  
  public Agent(int metabolism, int vision, int initialSugar, MovementRule mr, char sex){
    this.metabolism = metabolism;
    this.vision = vision;
    this.sugarLevel = initialSugar;
    assert(sex == 'X' || sex == 'Y');
    this.movementRule = mr;
    this.sex = sex;
    age = 0;
 //   lifespan = NOLIFESPAN;
    square = null;
    int[] tmp = {0, 0, 0};
    fillColor = tmp;
    snNode = null;
    initCulture();
  } 
  
  // returns a 0 or 1 using the random.nextInt function
  private int get0or1(){
    return rand.nextInt(2);
  } 
  
  private void initCulture(){
    // each cell of the culture array is set to a value selected uniformly at random
    culture = new boolean[11];
    for (int i = 0;i < culture.length;i++){
      culture[i] = get0or1()==0 ? false : true;  
    }  
  }  
  
  /*
   - picks a random number between 1 and 11. 
   If other's culture does not match this Agent's culture in the selected cultural attribute, 
   then mutate other's culture to match the culture of this agent
  */
  public void influence(Agent other){
    
    int index = rand.nextInt(11);
    // I think we only need to set others to ours.
    // If they don't match we will always set it
    // If they do match we will just set it to the value it was.
    other.setCultureAtIndex(index,culture[index]);
  } 
  
  public boolean getCultureAtIndex(int index){
    return culture[index]; 
  }  
  
  public void setCultureAtIndex(int index,boolean v){
    culture[index] = v;
  }
  
  /* 
  For each of the 11 dimensions of culture, 
    -- set this Agent's value for that dimension to be one of the two parent values, 
    -- selected uniformly at random. 
    -- Pick a different parent for each cultural dimension separately. 
  */
  
  public void nurture(Agent parent1, Agent parent2){
     Agent[] parents = new Agent[2];
     parents[0] = parent1;
     parents[1] = parent2;
     for(int i = 0; i<culture.length; i++){
       culture[i] = parents[get0or1()].getCultureAtIndex(i);  
     }  
  }  
  
  /*
    Returns true only if this Agent's culture contains more true values than false values
  */
  
  public boolean getTribe(){
    int numberOfTrues = 0;
    for (int i=0;i< culture.length;i++){
      if (culture[i])
        numberOfTrues++;
    }
    return (numberOfTrues > culture.length/2);  // greateer than half are true 
  }  
  
  /* returns the amount of food the agent needs to eat each turn to survive. 
  *
  */
  public int getMetabolism() {
    return metabolism; 
  } 
  
  /* returns the agent's vision radius.
  *
  */
  public int getVision() {
    return vision; 
  } 
  
  /* returns the amount of stored sugar the agent has right now.
  *
  */
  public int getSugarLevel() {
    return sugarLevel; 
  } 
  
  /* public char getSex() returns agents gender (X or Y)
  */
  public char getSex(){
    return sex; 
  }
  
  /* -  Provided that this agent has at least amount sugar, transfers that amount from this agent to the other 
  agent. (Parents use this to give sugar to their newborn children.) 
  If there's not enough sugar, then throw an AssertionError.
  */
   public void gift(Agent other, int amount){
     assert amount <= getSugarLevel() : "Sugar level too low";
     sugarLevel -= amount;
     other.addSugar(amount);
   }
  
  /* adds an amount of sugar to the agent
  */
   public void addSugar(int amount){
     sugarLevel += amount;
   }  
  
  /* returns the Agent's movement rule.
  *
  */
  public MovementRule getMovementRule() {
    return movementRule; 
  } 
  
  /* returns the Agent's age.
  *
  */
  public int getAge() {
    return age; 
  } 
  
  /* sets the Agent's age.
  *
  */
  public void setAge(int howOld) {
    assert(howOld >= 0);
    this.age = howOld; 
  } 
  
  /* returns the Agent's lifespan.
  *
  */
/*  public int getLifespan() {
    return lifespan; 
  } 
  
  /* sets the Agent's lifespan.
  *
  */
/*  
  public void setLifespan(int span) {
    assert(span >= 0);
    this.lifespan = span; 
  } 
*/  
  /* returns the Square occupied by the Agent.
  *
  */
  public Square getSquare() {
    return square; 
  } 
  
  /* sets the the Square occupied by the Agent.
  *
  */
  public void setSquare(Square s) {
    this.square = s; 
  } 
  
  /* sets the fill color to display this agent
   */
  public void setFillColor(int r, int g, int b) {
    int[] tmp = {r, g, b};
    fillColor = tmp;
  }
  
  /* gets the SocialNetworkNode
   */
  public SocialNetworkNode getSNNode() {
    return snNode;
  }
  
  /* sets the SocialNetworkNode
   */
  public void setSNNode(SocialNetworkNode node) {
    snNode = node;
  }
  
  /* Moves the agent from source to destination. 
  *  If the destination is already occupied, the program should crash with an assertion error
  *  instead, unless the destination is the same as the source.
  *
  */
  public void move(Square source, Square destination) {
    // make sure this agent occupies the source
    assert(this == source.getAgent());
    if (!destination.equals(source)) { 
      assert(destination.getAgent() == null);
      source.setAgent(null);
      destination.setAgent(this);
    }
  } 
  
  /* Reduces the agent's stored sugar level by its metabolic rate, to a minimum value of 0.
  *
  */
  public void step() {
    sugarLevel = Math.max(0, sugarLevel - metabolism); 
    age += 1;
  } 
  
  /* returns true if the agent's stored sugar level is greater than 0, false otherwise. 
  * 
  */
  public boolean isAlive() {
    return (sugarLevel > 0);
  } 
  
  /* The agent eats all the sugar at its Square. 
  *  The agent's sugar level is increased by that amount, and 
  *  the amount of sugar on the square is set to 0.
  *
  */
  public void eat() {
    sugarLevel += getSquare().getSugar();
    getSquare().setSugar(0);
  } 
  
  /* Two agents are equal only if they're the same agent, 
  *  not just if they have the same properties.
  */
  public boolean equals(Agent other) {
    return this == other;
  }
  
  public void display(int x, int y, int scale) {

     if(getTribe())
      fillColor=red;
    else
      fillColor=blue;
      
    fill(fillColor[0], fillColor[1], fillColor[2]);      
    ellipse(x, y, 3.0*scale/4, 3.0*scale/4);
  }
  
  /* compares the raster index x + width*y of this Agent's square to that of the other Agent's square
   *  - width is chosen to be something larger than the likely width of a SugarGrid to avoid ties.
   *
   */
  public int compareTo(Agent other) {
    Integer myVal = new Integer(square.getX() + Agent.MAXWIDTH*square.getX());
    Integer otherVal = new Integer(other.square.getX() + Agent.MAXWIDTH*other.square.getX());
    return myVal.compareTo(otherVal);
  }
}

class AgentTester {
  
  public void test() {
    
    // test constructor, accessors
    int metabolism = 3;
    int vision = 2;
    int initialSugar = 4;
    MovementRule m = null;
    Agent a = new Agent(metabolism, vision, initialSugar, m);
    assert(a.isAlive());
    assert(a.getMetabolism() == 3);
    assert(a.getVision() == 2);
    assert(a.getSugarLevel() == 4);
    assert(a.getMovementRule() == null);
    
    // movement
    Square s1 = new Square(5, 9, 10, 10);
    Square s2 = new Square(5, 9, 12, 12);
    s1.setAgent(a);
    a.move(s1, s2);
    assert(s2.getAgent().equals(a));
    
    // eat
    a.eat();
    assert(a.getSugarLevel() == 9);
    
    // test get/set MovementRule
    
    // step
    a.step();
    assert(a.getSugarLevel() == 6);
    a.step();
    a.step();
    a.step();
    assert(a.getSugarLevel() == 0);
    assert(!a.isAlive());
  }
}
