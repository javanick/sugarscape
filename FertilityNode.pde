
import java.util.Collections;
import java.util.Random;
import java.util.HashMap;
import java.util.Map;

class FertilityNode { 
  
  private int c = 0;
  private int o = 0;
  private int sugarLevel = 0;

    public FertilityNode(){
  }
  
  public FertilityNode(int c,int o,int sugarLevel){
    this.c = c;
    this.o = o;
    this.sugarLevel = sugarLevel;
  }
  
  public int getC(){
    return c;
  }  
  public int getO(){
    return o;
  }    
  public int getSugarLevel(){
    return sugarLevel;
  } 
  public void setC(int c){
    this.c = c; 
  }  
  public void setO(int o){
    this.o = o; 
  }  
  public void setSugarLevel(int level){
    this.sugarLevel = level;
  }  
   
}  
  
